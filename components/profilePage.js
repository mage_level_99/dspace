
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight } from 'react-native';
// import { Ionicons } from '@expo/vector-icons';
import { List, ListItem } from 'react-native-elements'

// import GenerateQR from '../../../reactJS/double-space/components/generateQR';
import PageTemplate from './smallComponents/pageTemplate';
// import pic from '../assets/bigRate.png'


export default class Profile extends React.Component {

    render() {
      const { navigate } = this.props.navigation;
      navigateBack=()=>{
        navigate('Queue')
      }

      
      return(
        <React.Fragment>
         
         <PageTemplate title={'Profile Settings'} navigate={navigateBack} />
          
          {/*<Image source={pic} />*/}
          {/**  Frst Section */}

          <View style={{
                  //backgroundColor:'blue'
                  }}>
                  <Text style={styles.section}>Account Information</Text>
                  
          </View>
          <TouchableOpacity
                        onPress={()=>{navigate('EditAccount')}}
                        style={{position: 'absolute',
                        marginTop:'32%',
                        flex:1,
                        flexDirection: 'row',
                        flexWrap: 'wrap'}}>
          
              <View style={{
                            marginLeft:'5%',
                            flex:1,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            justifyContent: 'flex-start',
                            //backgroundColor:'yellow',
                            alignItems: 'center',
                            //borderRadius:50,
                            //height:'60%'
                            }} >
                {/* <Ionicons style={{marginLeft:'20%'}}  name="ios-person" size={72} /> */}
              </View>
              
              <View style={{paddingTop:50,
                            marginRight:'40%',
                            
                            flex:2,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            //backgroundColor: 'red'
                          }}
                            >
                <Text>Sylvester Stallone</Text>
                <Text>+1 646-897-0098</Text>
                <Text>SlyLone@gmail.com</Text>
              </View>
            
         
          </TouchableOpacity>

          {/**add line section */}
          
          <View style={{
                    //backgroundColor:'yellow', 
                    marginTop:'33%'
                    }}>
                  <Text style={styles.section}>Settings</Text>
            {/* <ListItem
              onPress={()=>{navigate('QR')}}
              title={`My Qr Code`}
              subtitle={`Print out your QR code to receive messages`}
              leftElement={<Ionicons  name="ios-qr-scanner" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />

            <ListItem
              onPress={()=>{navigate('Rewards')}}
              title={`My Rewards`}
              subtitle={`Check the status of your points to redeem for rewards`}
              leftElement={<Ionicons  name="ios-gift" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />

            <ListItem
              onPress={()=>{navigate('Privacy')}}
              title={`Privacy`}
              subtitle={`Adjust your privacy settings`}
              leftElement={<Ionicons  name="ios-settings" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />
            <ListItem
              onPress={()=>{navigate('QR')}}
              title={`Sign Out`}
              subtitle={`Adjust your privacy settings`}
              leftElement={<Ionicons  name="ios-close-circle" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            /> */}
            

          </View>
             


          
              

          {/** <GenerateQR /> */}
        </React.Fragment>
      );
    }
  }

  const styles = StyleSheet.create({

    option : {
      //position: 'absolute',
      marginLeft:'7%',
      alignSelf: 'stretch',
      width: '100%',
      height: '15%',
      //flexWrap: 'wrap',
      justifyContent:'space-between',
      //padding:20 
    },
    section : {
      fontSize:20,
      marginLeft:'5%'
    }

  })


  /**
   * 
   * 
   * 

                  <View style={styles.option}>
                    <Ionicons  name="ios-gift" size={32} />
                    <TouchableHighlight>
                      <Text>Rewards</Text>
                    </TouchableHighlight>
                  </View>



                  
   * 
   */