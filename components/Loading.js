import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
import auth from '@react-native-firebase/auth';
import { connect } from 'react-redux';

class Loading extends React.Component {

    componentDidMount() {
        auth().onAuthStateChanged(user => {
          user ? this.props.onModalOne() : '',
          console.log(user)
          this.props.navigation.navigate(user ? 'Home' : 'SignUp' )
        })
      }

  render() {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})



const mapStateToProps = (state) => {
  console.log(state)
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    onModalOne: () => dispatch({ type: "CLOSE_MODAL_12", value: false}),
   
  };
};

export default connect(mapStateToProps,
  mapDispachToProps
  )(Loading)