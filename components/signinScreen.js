import React, {Component} from 'react';
import {StyleSheet,Modal, Text, Button, View, TextInput, Image} from 'react-native';
import { connect } from 'react-redux';
import auth from '@react-native-firebase/auth';

class SignIn extends Component {

  state = { email: '', password: '', errorMessage: null }

  handleLogin = () => {
    const { email, password } = this.state
    
      auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('Home'))
      .catch(error => this.setState({ errorMessage: error.message }))
  }

  render() {
    return (
      <React.Fragment>
        <Modal
          animationType="fade"
          transparent={false}
          visible={this.props.reducer.signIn}
         
           
          >


<View style={styles.container}>


<View style={{backgroundColor: 'black', alignSelf: 'stretch',width: '100%', height:'15%'}}>

<Text 
                style={{
                    position: 'absolute',
                    marginTop: '8%',
                    marginLeft: '3%',
                    color: 'white', 
                    fontSize: 35}}
                    >
                         Sign In
                </Text>

</View>

<View style={styles.InputContainer}>
<Text>Login</Text>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          secureTextEntry
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />

</View>

    
<View style={styles.buttonContainer}>


<Button title="Login" onPress={() => this.handleLogin,
this.props.onSignIn()} />
        <Button
          title="Don't have an account? Sign Up"
          onPress={() => this.props.navigation.navigate('SignUp'),
          this.props.onSignIn()}
        />


<Button title="Submit" onPress={() => {
                  
                  
                }}/>
</View>

</View>


        </Modal>

        </React.Fragment>
    );
  }
}


const styles = StyleSheet.create({
    
  container: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  InputContainer: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  buttonContainer: {
      width: '40%',
      position: 'absolute',
  bottom:40
  },
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: '5%'
  },
  listedText:{
      marginTop:'3%',
    fontSize: 18
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
  },

 
});

const mapStateToProps = (state) => {
  console.log(state)
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    onSignIn: () => dispatch({ type: "SIGN_IN", signIn: false}),
   
  };
};

export default connect(mapStateToProps,
  mapDispachToProps
  )(SignIn)