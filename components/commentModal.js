import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight,
   View, Alert, TextInput, Button, StyleSheet} from 'react-native';

import PageTemplate from './smallComponents/pageTemplate';


class ScanModal extends Component {
  state = {
    modalVisible: this.props.modalVisible,
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {

    return (
     
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View >
            <PageTemplate title={'Custom Message'} navigate={this.props.closeCommentModal} />

            
            <View style={{backgroundColor:'white',flex:1, padding:10}} />
                    
            <View style={{padding: 20}}>
              <Text>Enter a custom message below. This will overide any of your automatic settings in your profile. If you leave this empty we will use an automatic message or one of your preset message:</Text>
            <View style={{backgroundColor:'white',flex:1, padding:10}} />
              <TextInput
                style={{ 
                    borderRadius: 10,
                    height: 40,
                    maxHeight: 80,
                    width: 350, 
                    borderColor: 'gray', 
                    borderWidth: 1, 
                    marginLeft: 10}}
                //onChangeText={text => onChangeText(text)}
                //value={value}
              />
               <View style={{backgroundColor:'white',flex:1, padding:10}} />
              <View style={styles.btnWrapper}>
                <Button
                  title={"Set Text Content"}
                  onPress= {this.props.closeCommentModal} 
                  >
                </Button>
              </View>
              
              
            </View>
          </View>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </TouchableHighlight>
      </View>
    );
  }
}



export default ScanModal;


const styles = StyleSheet.create({

  btn : {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:35,
    height:35,
    backgroundColor:'#fff',
    borderRadius:50,
    marginTop: 150
  },
  
  btnWrapper: {
    width: '40%',
    margin: 20
  },
})



