import React from 'react'
import { StyleSheet, Text, TextInput, View, Button } from 'react-native'
import auth from '@react-native-firebase/auth';

import FirstModal from './firstModal'
import SecondModal from './secondModal'
import ThirdModal from './thirdModal'


export default class SignUp extends React.Component {
    state = { email: '', password: '', errorMessage: null }


 
    handleSignUp = () => {
       
          auth()
          .createUserWithEmailAndPassword(this.state.email, this.state.password)
          .then(() => this.props.navigation.navigate('Profile'))
          .catch(error => this.setState({ errorMessage: error.message }))
      }


render() {
    return (
<React.Fragment>
      <FirstModal/>
      <SecondModal/>
      <ThirdModal/>

      <View style={styles.container}>
        <View style={{backgroundColor: 'black', alignSelf: 'stretch',width: '100%', height:'15%',
    justifyContent: 'center'}}>

        <Text 
                style={{
                    position: 'absolute',
                    marginTop: '8%',
                    marginLeft: '3%',
                    color: 'white', 
                    fontSize: 35}}
                    >
                         Registration
                </Text>

</View>

<View style={styles.InputContainer}>

        <Text style = {{marginBottom: '10%', fontSize: 25}}>Sign Up</Text>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
        <TextInput
          placeholder="Email"
          autoCapitalize="none"
          style={styles.input}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          secureTextEntry
          placeholder="Password"
          autoCapitalize="none"
          style={styles.input}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
</View>

    
<View style={styles.buttonContainer1}>

        <Button title="Sign Up" onPress={this.handleSignUp} disabled = {this.state.email === '' || this.state.password === ''? true : false} />
</View>
       <View style={styles.buttonContainer}> 
        <Button
          title="Login"
          onPress={() => this.props.navigation.navigate('Login')}
        />
     
     </View>
     </View>
     </React.Fragment>
    )
  }
}
const styles = StyleSheet.create({
    
  container: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  InputContainer: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  buttonContainer: {
      width: '55%',
      
  bottom:'10%'

  
  },
  buttonContainer1: {
    width: '55%',
    
    bottom:'15%'


},
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: '5%'
  },
  listedText:{
      marginTop:'3%',
    fontSize: 18
  },
  input: {
    width: 220,
    height: 44,
    padding: 10,
    borderWidth: 1.7,
    borderColor: 'black',
    marginBottom: 10,
  },


 
});