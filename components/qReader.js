import * as React from 'react';
import { Text, View, StyleSheet, Button,
    Animated, TouchableOpacity, TextInput, Image, Slider, Alert } from 'react-native';
//import * as Permissions from 'expo-permissions';
//import { BarCodeScanner } from 'expo-barcode-scanner';
//import { Camera } from 'expo-camera';

import send from '../assets/sendIcon.png';
import license from '../assets/license.png';
import CommentModal from './commentModal';

export default class BarcodeScannerExample extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
    commentModalShow: false,
    data: null,
    zoom: 0
  };

  // async componentDidMount() {
  //   this.getPermissionsAsync();
  // }

  // getPermissionsAsync = async () => {
  //   const { status } = await Permissions.askAsync(Permissions.CAMERA);
  //   this.setState({ hasCameraPermission: status === 'granted' });
  // };

  // onValueChange(value) {
  //   this.setState({ zoom: value });
  // }

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      
      <View style={styles.container}>

        {/* <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
          //zoom={this.state.zoom}
        /> */}
        
        <View><Text style={{color:'white', marginLeft: '20%', marginTop: '20%'}}>{this.state.zoom}</Text></View>
        
        <View style={styles.overlay}>
          <View style={styles.unfocusedContainer}></View>
          
          <View style ={styles.textBox}><Text style={styles.text}>Scan QR Code</Text></View>
          
          {/* BUTTONS COMMENT & PLATE */}
          <View style = {styles.btnWrapper}>
            <TouchableOpacity 
              onPress={this.handleMessage}
              style={{
                borderWidth:1,
                borderColor:'rgba(0,0,0,0.2)',
                alignItems:'center',
                justifyContent:'center',
                width:35,
                height:35,
                backgroundColor:'#fff',
                borderRadius:50
              }}>
                <Image source={send}/>
            </TouchableOpacity>


            <TouchableOpacity 
              onPress={this.plateView}
              style={{
                borderWidth:1,
                borderColor:'rgba(0,0,0,0.2)',
                alignItems:'center',
                justifyContent:'center',
                width:35,
                height:35,
                backgroundColor:'#fff',
                borderRadius:50
              }}>
                <Image source={license}/>
            </TouchableOpacity>
          </View>


        <View style={styles.middleContainer}>
          
          
          
          <View style={styles.unfocusedContainer}></View>
          <View style={styles.focusedContainer}></View>

          {/* CORNERS FOR VIEW IN QR CODE */}
          <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                height: 25,
                width: 25,
                borderColor: '#FFFFFF',
                borderLeftWidth: 3,
                borderTopWidth: 3,
                marginLeft: '12.5%'
                }} />

            <View style={{
                position: 'absolute',
                top: 0,
                right: 0,
                height: 25,
                width: 25,
                borderColor: '#FFFFFF',
                borderRightWidth: 3,
                borderTopWidth: 3,
                marginRight: '12.5%'
                }} />

            <View style={{
                position: 'absolute',
                bottom: 0,
                right: 0,
                height: 25,
                width: 25,
                borderColor: '#FFFFFF',
                borderRightWidth: 3,
                borderBottomWidth: 3,
                marginRight: '12.5%'
                }} />

            <View style={{
                position: 'absolute',
                bottom: 0,
                left: 0,
                height: 25,
                width: 25,
                borderColor: '#FFFFFF',
                borderLeftWidth: 3,
                borderBottomWidth: 3,
                marginLeft: '12.5%'
                }} />

        <View style={styles.unfocusedContainer}></View>
      

        </View>

        <View style={styles.unfocusedContainer}></View>
        
        </View>
        {scanned && (
          <Button style={{marginTop:'19uly0%'}} title={'Tap to Scan Again'} onPress={() => this.setState({ scanned: false })} />
        )}

        <Slider
          minimumTrackTintColor = "yellow"
          maximumTrackTintColor = "white"
          minimumValue={0}
          maximumValue={1}
          step={.1}
          value={0}
          style={{
            position: 'absolute',
            marginTop: '130%',
            marginLeft: '20%',
            width: 200
            }} 
          onValueChange={value => this.onValueChange(value)}

        />      
        
        {this.state.commentModalShow && ( <CommentModal 
                                              modalVisible={this.state.commentModalShow} 
                                              closeCommentModal={this.closeCommentModal} 
                                              navigate={this.props.navigate}  />)}
            
      </View>

    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true, zoom:0 });
    this.props.navigate('Queue')
    //alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    Alert.alert(`Message Sent!`,`Your message has been sent to the double parker, track your request on this page, rate the user's responsiveness and communicate with law enforcement if need be.`);
  };

  //TESTING
  handleMessage =() => {
    //alert('Choose one of our ready made messages or enter a custom message below. You can set your perferred messages in your profile page.')
    this.setState({ commentModalShow: true })
  }

  plateView =() => {
    Alert.alert('Coming Soon','License Plate Reading coming soon...')
  }

  closeCommentModal = () => {
    this.setState({ commentModalShow: false})
  }
}

const styles = StyleSheet.create({

  btn : {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:35,
    height:35,
    backgroundColor:'#fff',
    borderRadius:50,
    marginTop: 150
  },
  
  btnWrapper: {
    position: 'absolute',
    flexDirection: 'row',
    flex: 2,
    justifyContent:'space-between',
    padding:15,
    marginLeft: '70%',
    marginTop: '10%', 
  },

  sliderWrapper: {
    position: 'absolute',
    marginTop: '150%',
    flexDirection: 'row'
  },

  textBox: {
    position: 'absolute',
    flexDirection: 'row',
    flex: 1.5,
    marginLeft: '25%',
    marginTop: '30%', 
  },

  text: {
    position: "absolute",
    color: "white",
    textAlign: 'center',
    fontSize: 30
  },
  
  textInput: {
    position: 'absolute',
    flexDirection: 'row',
    flex: 1.5,
    marginLeft: '25%',
    marginTop: '150%', 
  },

  container: {
    flex: 1,
    position: 'relative',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  unfocusedContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  middleContainer: {
    flexDirection: 'row',
    flex: 1.5
  },
  focusedContainer: {
    flex: 6,
  },
  animationLineStyle: {
    height: 2,
    width: '100%',
    backgroundColor: 'red',
  },
  rescanIconContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  })


//{this.state.modalVisible && ( <SecondModal modalVisible={this.props.modalVisible} />)}


/**
 * 
 * 
 * 
 * <TouchableOpacity
            style={{
              borderWidth:1,
              borderColor:'rgba(0,0,0,0.2)',
              alignItems:'center',
              justifyContent:'center',
              width:50,
              height:50,
              backgroundColor:'#fff',
              borderRadius:50,
              marginTop: 150
            }}
          >
   
          </TouchableOpacity>
 * 
 * 
 * 
 */