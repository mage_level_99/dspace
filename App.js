

import React from 'react';
import { StyleSheet, Text, View,StatusBar,Header, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';

import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import auth from '@react-native-firebase/auth';

import ourReducer from './store/reducer';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { faAddressCard } from '@fortawesome/free-solid-svg-icons'
import { faComments } from '@fortawesome/free-solid-svg-icons'
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons'



// import Loading from './components/Loading'

import Home from './components/mainPage'
import Log from './components/loginPage'

import Login from './components/Login'
import SignUp from './components/Signup'
import Load from './components/Loading'

import Queue from './components/queuePage';
import Info from './components/infoPage';
import Profile from './components/profilePage';



const store = createStore(ourReducer);

export default class App extends React.Component {



  render() {


    
    return (
      <Provider store={ store }>
   

        <AppContainer />

      
        </Provider>
    );
  }
}

const bottomTabNavigator = createBottomTabNavigator(
  {
 
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          // <Icon name="home" size={25} color={tintColor} />
          <FontAwesomeIcon icon={ faHome } size={25} color={tintColor} />
        )
      }
    },
    Queue: {
      screen: Queue,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <FontAwesomeIcon icon={ faComments } size={25} color={tintColor} />
        )
      }
    },
    Info: {
      screen: Info,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <FontAwesomeIcon icon={ faQuestionCircle } size={25} color={tintColor} />
        )
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <FontAwesomeIcon icon={ faAddressCard } size={25} color={tintColor} />
        )
      }
    }
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: '#eb6e3d'
    }
  }
);



const RootSwitch = createSwitchNavigator({ Load, SignUp, Login, bottomTabNavigator  });

const AppContainer = createAppContainer(RootSwitch);




